To use the binding system, it is sufficient to place the component ModelControlBinderManager on the form. It is a completely new development and does not use the existing data binding.


The ModelControlBinderManager offers extensive functions that can establish a control -> model connection.

The system supports two update functions. 
1. a pass-through function that transfers all changes to the control to the model.
2. a confirmation function that transfers all changes to the model only after calling ( Apply ).

If the pass-through function is active and the model implements the INotifyPropertyChanged interface, changes in the model can also be transferred directly to the control.

For more information, see the Test Project