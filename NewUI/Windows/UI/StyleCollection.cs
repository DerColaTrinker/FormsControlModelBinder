﻿using System.Collections.Generic;

namespace System.Windows.UI
{
    public class StyleCollection
    {
        private StyleControl control;
        private List<StyleElement> _elements = new List<StyleElement>();

        public StyleCollection(StyleControl control)
        {
            this.control = control;
        }

        public int Count { get { return _elements.Count; } }
    }
}