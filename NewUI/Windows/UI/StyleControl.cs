﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace System.Windows.UI
{
    public class StyleControl : System.Windows.Forms.Control
    {
        public StyleControl()
        {
            SetStyle(Forms.ControlStyles.UserPaint, true);
            SetStyle(Forms.ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(Forms.ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(Forms.ControlStyles.ContainerControl, true);

            Styles = new StyleCollection(this);
        }

        internal new bool DoubleBuffered { get => base.DoubleBuffered; set => base.DoubleBuffered = value; }

        public StyleCollection Styles { get; private set; }

        protected override void OnPaint(PaintEventArgs e)
        {
            if(Styles.Count==0)
            {
                e.Graphics.FillRectangle(Drawing.Brushes.Blue, e.ClipRectangle);
            }
        }
    }
}
