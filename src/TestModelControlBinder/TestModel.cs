﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolWheel
{
    class TestModel : System.ComponentModel.INotifyPropertyChanged
    {
        private string _text;

        public TestModel()
        {
            Text = "Test Wert";
            SourceElements = new string[] { "Eins", "Zwei", "Drei" };
        }

        public string Text { get => _text; set { _text = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Text")); } }

        public string[] SourceElements { get; set; }

        public string[] SelectedItems { get; internal set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
