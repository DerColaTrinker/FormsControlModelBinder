﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolWheel
{
    public partial class Form1 : Form
    {
        private TestModel _testmodel;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _testmodel = new TestModel();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            bindingManager1.Apply();
        }
    }
}
