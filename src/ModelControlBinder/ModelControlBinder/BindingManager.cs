﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModelControlBinder
{
    public sealed class BindingManager : Component
    {
        private HashSet<BindingContainer> _containers = new HashSet<BindingContainer>();

        public BindingManager()
        { }

        protected override bool CanRaiseEvents => true;

        public void Apply()
        {
            foreach (var container in _containers)
            {
                container.Apply();
            }
        }

        public bool ValidateAndApply()
        {
            var validation = Validate();
            var result = validation.Count() == 0;

            if (result)
                Apply();
            else
                MessageBox.Show(string.Join("\r\n", validation), "", MessageBoxButtons.OK, MessageBoxIcon.Information);

            return result;
        }

        public IEnumerable<string> Validate()
        {
            foreach (var container in _containers)
            {
                if (!container.Validate())
                    yield return container.Settings.ValidationMessage;
            }
        }

        public BindingSetting<TControl, TControlProperty, TModel, TModelProperty> CreateBinding<TControl, TControlProperty, TModel, TModelProperty>(TControl control, TModel model, Expression<Func<TControl, TControlProperty>> controlexpression, Expression<Func<TModel, TModelProperty>> modelexpression, Action<TControl, EventHandler> action)
          where TControl : Control
        {
            var controlpropertyinfo = GetControlPropertyInfo(controlexpression);
            var modelpropertyinfo = GetModelPropertyInfo(modelexpression);

            var binding = new BindingContainer<TControl, TControlProperty, TModel, TModelProperty>(control, controlpropertyinfo, model, modelpropertyinfo, action);
            binding.Initialize();

            _containers.Add(binding);

            return binding.Settings;
        }
        
        public void CreateListItemsBinding<TModel, TElement>(ListControl control, TModel model, Expression<Func<TModel, IEnumerable<TElement>>> modelexpression)
        {
            var modelpropertyinfo = GetModelPropertyInfo(modelexpression);
            var controlpropertinfo = default(PropertyInfo);
            var binding = default(BindingContainer);

            // Da alle Steuerelemente die eine Objektliste haben, eine eigene Collection als interne Klasse besitzen muss ich das hier unterscheiden.
            if (control is ListBox listbox)
            {
                controlpropertinfo = GetControlPropertyInfo<ListBox, ListBox.ObjectCollection>((c) => c.Items);

                binding = new BindingListItemsContainer<ListControl, ListBox.ObjectCollection, TModel, TElement>(control, controlpropertinfo, model, modelpropertyinfo, null);
            }

            if (control is ComboBox combobox)
            {
                controlpropertinfo = GetControlPropertyInfo<ComboBox, ComboBox.ObjectCollection>((c) => c.Items);

                binding = new BindingListItemsContainer<ListControl, ComboBox.ObjectCollection, TModel, TElement>(control, controlpropertinfo, model, modelpropertyinfo, null);
            }

            binding.Initialize();

            _containers.Add(binding);
        }

        public void CreateSelectedItemsBinding<TModel, TModelProperty>(ListControl control, TModel model, Expression<Func<TModel, IEnumerable<TModelProperty>>> modelexpression)
        {
            var modelpropertyinfo = GetModelPropertyInfo(modelexpression);
            var controlpropertinfo = default(PropertyInfo);
            var binding = default(BindingContainer);

            // Da alle Steuerelemente die eine Objektliste haben, eine eigene Collection als interne Klasse besitzen muss ich das hier unterscheiden.
            if (control is ListBox listbox)
            {
                controlpropertinfo = GetControlPropertyInfo<ListBox, ListBox.SelectedObjectCollection>((c) => c.SelectedItems);

                binding = new BindingSelectedItemsContainer<ListControl, ListBox.SelectedObjectCollection, TModel, TModelProperty>(control, controlpropertinfo, model, modelpropertyinfo, null);
            }

            //if (control is ComboBox combobox)
            //{
            //    controlpropertinfo = GetControlPropertyInfo<ComboBox, ComboBox.ObjectCollection>((c) => c.Items);

            //    binding = new BindingSelectedItemsContainer<ListControl, ComboBox.ObjectCollection, TModel, TModelProperty>(control, controlpropertinfo, model, targetpropertyinfo, null);
            //}

            binding.Initialize();

            _containers.Add(binding);
        }

        public void CreateCheckedItemsBinding<TModel, TModelProperty>(CheckedListBox control, TModel model, Expression<Func<TModel, IEnumerable<TModelProperty>>> modelexpression)
        {
            var modelpropertyinfo = GetModelPropertyInfo(modelexpression);
            var controlpropertinfo = GetControlPropertyInfo<CheckedListBox, CheckedListBox.CheckedItemCollection>((c) => c.CheckedItems);

            var binding = new BindingCheckedItemsContainer<CheckedListBox, CheckedListBox.CheckedItemCollection, TModel, TModelProperty>(control, controlpropertinfo, model, modelpropertyinfo, null);

            binding.Initialize();

            _containers.Add(binding);
        }

        internal PropertyInfo GetControlPropertyInfo<TControl, TProperty>(Expression<Func<TControl, TProperty>> exp)
        {
            if (exp.Body is MemberExpression body)
            {
                if (body.Member is PropertyInfo property)
                {
                    return property;
                }
            }

            return null;
        }

        internal PropertyInfo GetModelPropertyInfo<TModel, TProperty>(Expression<Func<TModel, TProperty>> exp)
        {
            if (exp.Body is MemberExpression body)
            {
                if (body.Member is PropertyInfo property)
                {
                    return property;
                }
            }

            return null;
        }
    }
}