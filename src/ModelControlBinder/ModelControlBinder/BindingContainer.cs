﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModelControlBinder
{
    public abstract class BindingContainer
    {
        /// <summary>
        /// Creates a new instance of the ModelBinding class
        /// </summary>
        /// <param name="control"></param>
        /// <param name="controlpropertyinfo"></param>
        /// <param name="model"></param>
        /// <param name="modelpropertyinfo"></param>
        /// <param name="modeltype"></param>
        public BindingContainer(Control control, PropertyInfo controlpropertyinfo, object model, PropertyInfo modelpropertyinfo)
        {
            Control = control;
            ControlPropertyInfo = controlpropertyinfo;
            Model = model;
            ModelpropertyInfo = modelpropertyinfo;
        }

        internal abstract void Initialize();

        public BindingSetting Settings { get; internal set; }

        /// <summary>
        /// Returns the control that is bound
        /// </summary>
        public Control Control { get; }

        /// <summary>
        /// Returns the property of the control that is bound
        /// </summary>
        public PropertyInfo ControlPropertyInfo { get; }

        /// <summary>
        /// Return the mode that is bound
        /// </summary>
        public object Model { get; }

        /// <summary>
        /// Returns the property of the model that is bound
        /// </summary>
        public PropertyInfo ModelpropertyInfo { get; }

        protected abstract void ModelPropertyChanged();

        protected abstract void ControlPropertyChanged();

        /// <summary>
        /// If overwritten, changes in the control are transferred to the model.
        /// </summary>
        public abstract void Apply();

        public abstract bool Validate();

        /// <summary>
        /// If overwritten, changes in the control are reset from the model.
        /// </summary>
        public abstract void Reset();
    }

    public class BindingContainer<TControl, TControlProperty, TModel, TModelProperty> : BindingContainer
          where TControl : Control
    {
        private readonly EventHandler _eventhandler;
        private bool _lock;

        /// <summary>
        /// Creates a new instance of the ModelBinding class
        /// </summary>
        /// <param name="control"></param>
        /// <param name="controlpropertyinfo"></param>
        /// <param name="controltype"></param>
        /// <param name="model"></param>
        /// <param name="modelpropertyinfo"></param>
        /// <param name="modeltype"></param>
        /// <param name="action"></param>
        public BindingContainer(TControl control, PropertyInfo controlpropertyinfo, TModel model, PropertyInfo modelpropertyinfo, Action<TControl, EventHandler> action)
            : base(control, controlpropertyinfo, model, modelpropertyinfo)
        {
            Action = action;

            // Steuerelement Event umleiten
            _eventhandler = new EventHandler(InternalControlPropertyChanged);

            // Event aus dem Model umleiten
            if (model is INotifyPropertyChanged propertynotify)
            {
                propertynotify.PropertyChanged += InternalModelPropertyChanged;
            }

            Settings = new BindingSetting<TControl, TControlProperty, TModel, TModelProperty>();

            if (action != null)
                action.Invoke(control, _eventhandler);
        }

        /// <summary>
        /// Returns the redirected event when changes are made to the control.
        /// </summary>
        public Action<TControl, EventHandler> Action { get; }

        public new TControl Control { get { return (TControl)base.Control; } }

        public new TModel Model { get { return (TModel)base.Model; } }

        private void InternalModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (_lock) return;
            if (e.PropertyName != ModelpropertyInfo.Name) return;

            ModelPropertyChanged();
        }

        protected override void ModelPropertyChanged()
        {
            EnterLock();
            Settings.ActionOnChangeModelValueTarget?.Invoke();
            ControlValue = CostumConverter(ModelValue);
            ReleaseLock();
        }

        private void InternalControlPropertyChanged(object sender, EventArgs e)
        {
            if (_lock) return;

            ControlPropertyChanged();
        }

        protected override void ControlPropertyChanged()
        {
            EnterLock();
            Settings.ActionOnChangeControlValueTarget?.Invoke();
            ModelValue = CostumConverter(ControlValue);
            ReleaseLock();
        }

        internal override void Initialize()
        {
            EnterLock();
            ControlValue = CostumConverter(ModelValue);
            ReleaseLock();
        }

        /// <summary>
        /// Resolves a lock to further process the model or control events.
        /// </summary>
        protected void ReleaseLock()
        {
            _lock = false;
        }

        /// <summary>
        /// Sets a lock so that the model or control events are not processed.
        /// </summary>
        protected void EnterLock()
        {
            _lock = true;
        }

        /// <summary>
        /// Allows you to convert model to control property values
        /// </summary>
        /// <param name="modelValue"></param>
        /// <returns></returns>
        protected virtual TControlProperty CostumConverter(TModelProperty modelValue)
        {
            return (TControlProperty)(object)modelValue;
        }

        /// <summary>
        /// Allows you to convert control to model property values
        /// </summary>
        /// <param name="modelValue"></param>
        /// <returns></returns>
        protected virtual TModelProperty CostumConverter(TControlProperty modelValue)
        {
            return (TModelProperty)(object)modelValue;
        }

        public override bool Validate()
        {
            if (Settings.ValidationTarget == null) return true;

            return Settings.ValidationTarget.Invoke(Model, ControlValue);
        }

        /// <summary>
        /// Changes in the control are transferred to the model.
        /// </summary>
        public override void Apply()
        {
            EnterLock();
            ModelValue = CostumConverter(ControlValue);
            ReleaseLock();
        }

        /// <summary>
        /// Changes in the control are reset from the model.
        /// </summary>
        public override void Reset()
        {
            EnterLock();
            ControlValue = CostumConverter(ModelValue);
            ReleaseLock();
        }

        /// <summary>
        /// Returns or sets the value from the control property.
        /// </summary>
        public virtual TControlProperty ControlValue
        {
            get { return (TControlProperty)ControlPropertyInfo.GetValue(Control); }
            set { ControlPropertyInfo.SetValue(Control, value); }
        }

        /// <summary>
        /// Returns or sets the value from the model property.
        /// </summary>
        public virtual TModelProperty ModelValue
        {
            get { return (TModelProperty)ModelpropertyInfo.GetValue(Model); }
            set { ModelpropertyInfo.SetValue(Model, value); }
        }

        public new BindingSetting<TControl, TControlProperty, TModel, TModelProperty> Settings { get; }
    }

    public class BindingListItemsContainer<TControl, TControlProperty, TModel, TModelProperty> : BindingContainer<TControl, TControlProperty, TModel, IEnumerable<TModelProperty>>
        where TControl : ListControl
    {
        public BindingListItemsContainer(TControl control, PropertyInfo controlpropertyinfo, TModel model, PropertyInfo modelpropertyinfo, Action<TControl, EventHandler> action)
            : base(control, controlpropertyinfo, model, modelpropertyinfo, action)
        {
            ElementPropertyInfo = modelpropertyinfo;
        }

        public PropertyInfo ElementPropertyInfo { get; }

        public IEnumerable<TModelProperty> Elements { get { return (IEnumerable<TModelProperty>)ElementPropertyInfo.GetValue(Model); } }

        internal override void Initialize()
        {
            // Sicherstellen das es eine ListBox ist
            if (Control is ListBox listbox)
            {
                listbox.Items.Clear();
                listbox.Items.AddRange(Elements.Cast<object>().ToArray());
            }

            //base.Initialize();
        }

        public override void Apply()
        {
            if (ControlValue is ListBox.ObjectCollection listbox)
            {
                var result = listbox.Cast<TModelProperty>();

                // Bei einem Array machen wir eine Ausnahme, und erzeugen den Type neu
                if (ModelValue is TModelProperty[] | ModelpropertyInfo.PropertyType.IsArray)
                {
                    ModelValue = result.ToArray();
                    return;
                }

                if (ModelValue is IList<TModelProperty> ilist)
                {
                    ilist.Clear();
                    foreach (var item in result) ilist.Add(item);
                    return;
                }

                if (ModelValue is ISet<TModelProperty> iset)
                {
                    iset.Clear();
                    foreach (var item in result) iset.Add(item);

                    return;
                }
            }

            if (ControlValue is ComboBox.ObjectCollection combobox)
            {
                var result = combobox.Cast<TModelProperty>();

                // Bei einem Array machen wir eine Ausnahme, und erzeugen den Type neu
                if (ModelValue is TModelProperty[] | ModelpropertyInfo.PropertyType.IsArray)
                {
                    ModelValue = result;
                    return;
                }

                if (ModelValue is IList<TModelProperty> ilist)
                {
                    ilist.Clear();
                    foreach (var item in result) ilist.Add(item);
                    return;
                }

                if (ModelValue is ISet<TModelProperty> iset)
                {
                    iset.Clear();
                    foreach (var item in result) iset.Add(item);

                    return;
                }
            }
        }
    }

    public class BindingSelectedItemsContainer<TControl, TControlProperty, TModel, TModelProperty> : BindingContainer<TControl, TControlProperty, TModel, IEnumerable<TModelProperty>>
            where TControl : ListControl
    {
        public BindingSelectedItemsContainer(TControl control, PropertyInfo controlpropertyinfo, TModel model, PropertyInfo modelpropertyinfo, Action<TControl, EventHandler> action)
            : base(control, controlpropertyinfo, model, modelpropertyinfo, action)
        { }

        public PropertyInfo ElementPropertyInfo { get; }

        public IEnumerable<TModelProperty> Elements { get { return (IEnumerable<TModelProperty>)ElementPropertyInfo.GetValue(Model); } }

        internal override void Initialize()
        {
            // Sicherstellen das es eine ListBox ist
            if (ControlValue is ListBox.SelectedObjectCollection listbox)
            {
                var result = listbox.Cast<TModelProperty>();

                if (ModelValue is TModelProperty[] array) foreach (var item in array) listbox.Add(item);
                if (ModelValue is IList<TModelProperty> ilist) foreach (var item in ilist) listbox.Add(item);
                if (ModelValue is ISet<TModelProperty> iset) foreach (var item in iset) listbox.Add(item);
            }

            //base.Initialize();
        }

        protected override void ControlPropertyChanged()
        {
            // Nichts machen, Änderungen durch das Control werden nicht ausgewertet.
        }

        protected override void ModelPropertyChanged()
        {
            if (ControlValue is ListBox.SelectedObjectCollection listbox)
            {
                var result = listbox.Cast<TModelProperty>();

                if (ModelValue is TModelProperty[] array) foreach (var item in array) listbox.Add(item);
                if (ModelValue is IList<TModelProperty> ilist) foreach (var item in ilist) listbox.Add(item);
                if (ModelValue is ISet<TModelProperty> iset) foreach (var item in iset) listbox.Add(item);
            }
        }

        public override void Apply()
        {
            if (ControlValue is ListBox.SelectedObjectCollection listbox)
            {
                var result = listbox.Cast<TModelProperty>();

                // Bei einem Array machen wir eine Ausnahme, und erzeugen den Type neu
                if (ModelValue is TModelProperty[] | ModelpropertyInfo.PropertyType.IsArray)
                {
                    ModelValue = result.ToArray();
                    return;
                }

                if (ModelValue is IList<TModelProperty> ilist)
                {
                    ilist.Clear();
                    foreach (var item in result) ilist.Add(item);
                    return;
                }

                if (ModelValue is ISet<TModelProperty> iset)
                {
                    iset.Clear();
                    foreach (var item in result) iset.Add(item);

                    return;
                }
            }
        }
    }

    public class BindingCheckedItemsContainer<TControl, TControlProperty, TModel, TModelProperty> : BindingContainer<TControl, TControlProperty, TModel, IEnumerable<TModelProperty>>
            where TControl : CheckedListBox
    {
        public BindingCheckedItemsContainer(TControl control, PropertyInfo controlpropertyinfo, TModel model, PropertyInfo modelpropertyinfo, Action<TControl, EventHandler> action)
            : base(control, controlpropertyinfo, model, modelpropertyinfo, action)
        { }

        public PropertyInfo ElementPropertyInfo { get; }

        public IEnumerable<TModelProperty> Elements { get { return (IEnumerable<TModelProperty>)ElementPropertyInfo.GetValue(Model); } }

        internal override void Initialize()
        {
            // Sicherstellen das es eine ListBox ist
            if (ControlValue is CheckedListBox.CheckedItemCollection listbox)
            {
                var result = listbox.Cast<TModelProperty>();

                Control.ClearSelected();

                if (ModelValue is TModelProperty[] array)
                {
                    foreach (var item in array)
                    {
                        var index = Control.Items.IndexOf(item);
                        if (index > -1) Control.SetItemChecked(index, true);
                    }
                }

                if (ModelValue is IList<TModelProperty> ilist)
                {
                    foreach (var item in ilist)
                    {
                        var index = Control.Items.IndexOf(item);
                        if (index > -1) Control.SetItemChecked(index, true);
                    }
                }

                if (ModelValue is ISet<TModelProperty> iset)
                {
                    foreach (var item in iset)
                    {
                        var index = Control.Items.IndexOf(item);
                        if (index > -1) Control.SetItemChecked(index, true);
                    }
                }
            }

            //base.Initialize();
        }

        protected override void ControlPropertyChanged()
        {
            // Nichts machen, Änderungen durch das Control werden nicht ausgewertet.
        }

        protected override void ModelPropertyChanged()
        {
            if (ControlValue is CheckedListBox.CheckedItemCollection listbox)
            {
                var result = listbox.Cast<TModelProperty>();

                if (ModelValue is TModelProperty[] array)
                {
                    foreach (var item in array)
                    {
                        var index = Control.Items.IndexOf(item);
                        if (index > -1) Control.SetItemChecked(index, true);
                    }
                }

                if (ModelValue is IList<TModelProperty> ilist)
                {
                    foreach (var item in ilist)
                    {
                        var index = Control.Items.IndexOf(item);
                        if (index > -1) Control.SetItemChecked(index, true);
                    }
                }

                if (ModelValue is ISet<TModelProperty> iset)
                {
                    foreach (var item in iset)
                    {
                        var index = Control.Items.IndexOf(item);
                        if (index > -1) Control.SetItemChecked(index, true);
                    }
                }
            }
        }

        public override void Apply()
        {
            if (ControlValue is CheckedListBox.CheckedItemCollection listbox)
            {
                var result = listbox.Cast<TModelProperty>();

                // Bei einem Array machen wir eine Ausnahme, und erzeugen den Type neu
                if (ModelValue is TModelProperty[] | ModelpropertyInfo.PropertyType.IsArray)
                {
                    ModelValue = result.ToArray();
                    return;
                }

                if (ModelValue is IList<TModelProperty> ilist)
                {
                    ilist.Clear();
                    foreach (var item in result) ilist.Add(item);
                    return;
                }

                if (ModelValue is ISet<TModelProperty> iset)
                {
                    iset.Clear();
                    foreach (var item in result) iset.Add(item);

                    return;
                }
            }
        }
    }


}