﻿using ModelControlBinder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace System.Windows.Forms
{
    public static class BindingExtentions
    {
        #region Control - Bindings

        public static BindingSetting<Control, string, TModel, string> TextBinding<TModel>(this Control control, TModel model, Expression<Func<TModel, string>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.Text, modelproperty, (c, eh) => c.TextChanged += eh);
        }

        public static BindingSetting<Control, Color, TModel, Color> BackcolorBinding<TModel>(this Control control, TModel model, Expression<Func<TModel, Color>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.BackColor, modelproperty, (c, eh) => c.BackColorChanged += eh);
        }

        public static BindingSetting<Control, Color, TModel, Color> ForecolorBinding<TModel>(this Control control, TModel model, Expression<Func<TModel, Color>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.ForeColor, modelproperty, (c, eh) => c.ForeColorChanged += eh);
        }

        public static BindingSetting<Control, bool, TModel, bool> EnabledBinding<TModel>(this Control control, TModel model, Expression<Func<TModel, bool>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.Enabled, modelproperty, (c, eh) => c.EnabledChanged += eh);
        }

        public static BindingSetting<Control, bool, TModel, bool> VisibleBinding<TModel>(this Control control, TModel model, Expression<Func<TModel, bool>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.Visible, modelproperty, (c, eh) => c.VisibleChanged += eh);
        }

        public static BindingSetting<Control, Point, TModel, Point> LocationBinding<TModel>(this Control control, TModel model, Expression<Func<TModel, Point>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.Location, modelproperty, (c, eh) => c.LocationChanged += eh);
        }

        public static BindingSetting<Control, Size, TModel, Size> SizeBinding<TModel>(this Control control, TModel model, Expression<Func<TModel, Size>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.Size, modelproperty, (c, eh) => c.SizeChanged += eh);
        }

        public static BindingSetting<Control, Image, TModel, Image> SizeBinding<TModel>(this Control control, TModel model, Expression<Func<TModel, Image>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.BackgroundImage, modelproperty, (c, eh) => c.BackgroundImageChanged += eh);
        }

        public static BindingSetting<Control, ImageLayout, TModel, ImageLayout> SizeBinding<TModel>(this Control control, TModel model, Expression<Func<TModel, ImageLayout>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.BackgroundImageLayout, modelproperty, (c, eh) => c.BackgroundImageLayoutChanged += eh);
        }

        public static BindingSetting<Control, Cursor, TModel, Cursor> SizeBinding<TModel>(this Control control, TModel model, Expression<Func<TModel, Cursor>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.Cursor, modelproperty, (c, eh) => c.CursorChanged += eh);
        }

        #endregion

        #region CheckBox - Bindings

        public static BindingSetting<CheckBox, bool, TModel, bool> CheckBinding<TModel>(this CheckBox control, TModel model, Expression<Func<TModel, bool>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.Checked, modelproperty, (c, eh) => c.CheckedChanged += eh);
        }

        public static BindingSetting<CheckBox, CheckState, TModel, CheckState> CheckStateBinding<TModel>(this CheckBox control, TModel model, Expression<Func<TModel, CheckState>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.CheckState, modelproperty, (c, eh) => c.CheckStateChanged += eh);
        }

        #endregion

        #region DateTimePicker

        public static BindingSetting<DateTimePicker, DateTime, TModel, DateTime> ValueBinding<TModel>(this DateTimePicker control, TModel model, Expression<Func<TModel, DateTime>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.Value, modelproperty, (c, eh) => c.ValueChanged += eh);
        }

        #endregion

        #region ListControl - Bindings

        public static void ListItemsBinding<TModel, TModelProperty>(this ListControl control, TModel model, Expression<Func<TModel, IEnumerable<TModelProperty>>> modelexpression)
        {
            CreateListItemsBinding(control, model, modelexpression);
        }

        public static void SelectedItemsBinding<TModel, TModelProperty>(this ListControl control, TModel model, Expression<Func<TModel, IEnumerable<TModelProperty>>> modelexpression)
        {
            if (control is ComboBox) throw new BindingException("ComboBox not supported");
            CreateSelectedItemsBinding(control, model, modelexpression);
        }

        #region ListBox - Binding

        public static BindingSetting<ListBox, object, TModel, TModelProperty> SelectedItemBinding<TModel, TModelProperty>(this ListBox control, TModel model, Expression<Func<TModel, TModelProperty>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.SelectedItem, modelproperty, (c, eh) => c.SelectedIndexChanged += eh);
        }

        #endregion

        #region ComboBox - Binding

        public static BindingSetting<ComboBox, object, TModel, TModelProperty> SelectedItemBinding<TModel, TModelProperty>(this ComboBox control, TModel model, Expression<Func<TModel, TModelProperty>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.SelectedItem, modelproperty, (c, eh) => c.SelectedIndexChanged += eh);
        }

        public static BindingSetting<ComboBox, string, TModel, string> SelectedTextBinding<TModel>(this ComboBox control, TModel model, Expression<Func<TModel, string>> modelproperty)
        {
            return CreateBinding(control, model, (c) => c.SelectedText, modelproperty, (c, eh) => c.SelectionChangeCommitted += eh);
        }

        #endregion

        #region CheckedListBox

        public static void SelectedItemsBinding<TModel, TModelProperty>(this CheckedListBox control, TModel model, Expression<Func<TModel, IEnumerable<TModelProperty>>> modelexpression)
        {
            CreateSelectedItemsBinding(control, model, modelexpression);
        }

        #endregion

        #endregion

        public static BindingSetting<TControl, TControlProperty, TModel, TModelProperty> CreateBinding<TControl, TControlProperty, TModel, TModelProperty>(this TControl control, TModel model, Expression<Func<TControl, TControlProperty>> controlexpression, Expression<Func<TModel, TModelProperty>> modelexpression, Action<TControl, EventHandler> action)
            where TControl : Control
        {
            // Nach der BindingManager-Komponente auf der Form suchen. Die das Binding aufnehmen kann.
            var manager = FindBindingManager(control);

            // Wenn nicht gefunden einen Fehler ausgeben.
            if (manager == null) throw new BindingException("Binding Manager not found. Please place the BindingManager component on the form.");

            return manager.CreateBinding(control, model, controlexpression, modelexpression, action);
        }

        public static void CreateListItemsBinding<TControl, TModel, TModelProperty>(this TControl control, TModel model, Expression<Func<TModel, IEnumerable<TModelProperty>>> modelexpression)
          where TControl : ListControl
        {
            // Nach der BindingManager-Komponente auf der Form suchen. Die das Binding aufnehmen kann.
            var manager = FindBindingManager(control);

            // Wenn nicht gefunden einen Fehler ausgeben.
            if (manager == null) throw new BindingException("Binding Manager not found. Please place the BindingManager component on the form.");

            manager.CreateListItemsBinding(control, model, modelexpression);
        }

        public static void CreateSelectedItemsBinding<TControl, TModel, TModelProperty>(this TControl control, TModel model, Expression<Func<TModel, IEnumerable<TModelProperty>>> modelexpression)
            where TControl : ListControl
        {
            // Nach der BindingManager-Komponente auf der Form suchen. Die das Binding aufnehmen kann.
            var manager = FindBindingManager(control);

            // Wenn nicht gefunden einen Fehler ausgeben.
            if (manager == null) throw new BindingException("Binding Manager not found. Please place the BindingManager component on the form.");

            manager.CreateSelectedItemsBinding(control, model, modelexpression);
        }

        public static void CreateCheckedItemsBinding<TControl, TModel, TModelProperty>(this TControl control, TModel model, Expression<Func<TModel, IEnumerable<TModelProperty>>> modelexpression)
            where TControl : CheckedListBox
        {
            // Nach der BindingManager-Komponente auf der Form suchen. Die das Binding aufnehmen kann.
            var manager = FindBindingManager(control);

            // Wenn nicht gefunden einen Fehler ausgeben.
            if (manager == null) throw new BindingException("Binding Manager not found. Please place the BindingManager component on the form.");

            manager.CreateCheckedItemsBinding(control, model, modelexpression);
        }

        private static BindingManager FindBindingManager<TControl>(TControl control) where TControl : Control
        {
            var form = control.FindForm();
            var manager = (from field in form.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                           where typeof(BindingManager).IsAssignableFrom(field.FieldType)
                           let component = (Component)field.GetValue(form)
                           where component != null
                           select component).Cast<BindingManager>().FirstOrDefault();
            return manager;
        }
    }
}
