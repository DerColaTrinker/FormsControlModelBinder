﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModelControlBinder
{
    public delegate bool ValidationDelegate<TModel, TControlProperty>(TModel model, TControlProperty property);

    public static class BindingSettingExtentions
    {
        public static BindingSetting<TControl, TControlProperty, TModel, TModelProperty> Validate<TControl, TControlProperty, TModel, TModelProperty>(this BindingSetting<TControl, TControlProperty, TModel, TModelProperty> setting, ValidationDelegate<TModel, TControlProperty> target, string message)
            where TControl : Control
        {
            setting.ValidationTarget = target;
            setting.ValidationMessage = message;

            return setting;
        }

        public static BindingSetting<TControl, TControlProperty, TModel, TModelProperty> ActionOnChangeControlValue<TControl, TControlProperty, TModel, TModelProperty>(this BindingSetting<TControl, TControlProperty, TModel, TModelProperty> setting, Action action)
            where TControl : Control
        {
            setting.ActionOnChangeControlValueTarget = action;

            return setting;
        }

        public static BindingSetting<TControl, TControlProperty, TModel, TModelProperty> ActionOnChangeModelValue<TControl, TControlProperty, TModel, TModelProperty>(this BindingSetting<TControl, TControlProperty, TModel, TModelProperty> setting, Action action)
            where TControl : Control
        {
            setting.ActionOnChangeModelValueTarget = action;

            return setting;
        }
    }
}