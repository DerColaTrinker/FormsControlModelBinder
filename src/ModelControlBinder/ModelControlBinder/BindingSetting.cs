﻿using System;
using System.Windows.Forms;

namespace ModelControlBinder
{
    public abstract class BindingSetting
    {
        internal string ValidationMessage { get; set; }

        internal Action ActionOnChangeControlValueTarget { get; set; }

        internal Action ActionOnChangeModelValueTarget { get; set; }
    }

    public sealed class BindingSetting<TControl, TControlProperty, TModel, TModelProperty> : BindingSetting
        where TControl : Control
    {
        internal ValidationDelegate<TModel, TControlProperty> ValidationTarget { get; set; }
    }
}